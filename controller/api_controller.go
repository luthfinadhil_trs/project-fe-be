package api

import (
	"github.com/gofiber/fiber/v2"
	// "fmt"
)

func Put(c *fiber.Ctx) error {
	return c.SendString("this is put")
}
func Patch(c *fiber.Ctx) error {
	return c.SendString("this is patch")
}

func Delete(c *fiber.Ctx) error {
	return c.SendString("String deleted")
}

func Get_Data(c *fiber.Ctx) error {

	kata := "ini method get"
	return c.SendString(kata)
}

func Post_Data(c *fiber.Ctx) error {
	return c.SendString("This is HTTP Method POST")
}
